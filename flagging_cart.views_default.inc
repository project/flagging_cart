<?php
/**
 * @file
 * Default flagging cart views.
 */

/**
 * Implements hook_views_default_views().
 */
function flagging_cart_views_default_views() {

  $view = new view();
  $view->name = 'flagging_cart';
  $view->description = 'Provide a list of items in a user\'s flagging cart';
  $view->tag = 'flagging_cart';
  $view->base_table = 'node';
  $view->human_name = 'Flagging Cart';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Cart';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'flagging_cart_quantity' => 'flagging_cart_quantity',
    'ops' => 'ops',
  );
  $handler->display->display_options['style_options']['default'] = 'title';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'flagging_cart_quantity' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'ops' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'Cart empty';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'Your cart is empty.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Relationship: Flags: flagging_cart */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['label'] = 'flagging_cart';
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'flagging_cart';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Item';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Flagging: Quantity */
  $handler->display->display_options['fields']['flagging_cart_quantity']['id'] = 'flagging_cart_quantity';
  $handler->display->display_options['fields']['flagging_cart_quantity']['table'] = 'field_data_flagging_cart_quantity';
  $handler->display->display_options['fields']['flagging_cart_quantity']['field'] = 'flagging_cart_quantity';
  $handler->display->display_options['fields']['flagging_cart_quantity']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['fields']['flagging_cart_quantity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['flagging_cart_quantity']['type'] = 'flagging_cart_quantity';
  $handler->display->display_options['fields']['flagging_cart_quantity']['settings'] = array(
    'empty_text' => '',
    'fallback_format' => 'number_integer',
    'fallback_settings' => array(
      'thousand_separator' => '',
      'prefix_suffix' => 1,
    ),
  );
  /* Field: Flags: Flag link */
  $handler->display->display_options['fields']['ops']['id'] = 'ops';
  $handler->display->display_options['fields']['ops']['table'] = 'flagging';
  $handler->display->display_options['fields']['ops']['field'] = 'ops';
  $handler->display->display_options['fields']['ops']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['fields']['ops']['label'] = '';
  $handler->display->display_options['fields']['ops']['element_label_colon'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  
  /* Display: Cart page */
  $handler = $view->new_display('page', 'Cart page', 'flagging_cart_page');
  $handler->display->display_options['display_description'] = 'Provides a page view of the user\'s cart.';
  $handler->display->display_options['path'] = 'cart';
  
  /* Display: Cart block */
  $handler = $view->new_display('block', 'Cart block', 'flagging_cart_block');
  $handler->display->display_options['display_description'] = 'Provides a block view of the user\'s cart.';

  $views[$view->name] = $view;
  return $views;
}
