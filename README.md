# Flagging Cart

The flagging cart module uses flags to simulate a shopping cart. The base module
simply provides a default flag type that can mark nodes as added to a user's
cart and record the quantity desired.

The flagging cart rules module provides a checkout button and rules that can be
triggered upon a user checkout.

The flagging cart product module provides a basic product content type with
title, description and price fields. It also adds the price and total fields
to the shopping cart.

The flagging cart stock module extends the flagging cart product module by
adding a stock field and default rule to track the number of products in stock.

The flagging cart order module provides an order node that will record the
order information upon checkout.

No payment or shipping modules will be created. Please use drupal commerce if
you require that additional functionality.

