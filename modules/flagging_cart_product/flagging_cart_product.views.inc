<?php
/**
 * @file
 * Flagging cart product views include.
 */

/**
 * Implments hook_views_data().
 *
 * Provide the product total handler.
 */
function flagging_cart_product_views_data() {
  $data['views']['flagging_cart_product_total'] = array(
    'title' => t('Total'),
    'help' => t('Provide the total price of all products in the cart.'),
    'area' => array(
      'handler' => 'flagging_cart_product_total_area_handler',
    ),
  );
  return $data;
}
