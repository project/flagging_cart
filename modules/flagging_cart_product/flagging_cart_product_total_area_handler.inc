<?php
/**
 * @file
 * Product total views handler.
 */

class flagging_cart_product_total_area_handler extends views_handler_area_text {

  function option_definition() {
    $options = parent::option_definition();

    $options['display_label'] = array('default' => 'Total: ');
    $options['separator'] = array('default' => ',');
    $options['prefix'] = array('default' => '');
    $options['suffix'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Remove the general text form settings.
    unset($form['empty']);
    unset($form['content']);
    unset($form['format']);
    unset($form['tokenize']);
    unset($form['token_help']);

    // Add the number format fields.
    $form['display_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Display label'),
      '#default_value' => $this->options['display_label'],
      '#description' => t('Text label to display before the numeric total.'),
    );
    $form['separator'] = array(
      '#type' => 'select',
      '#title' => t('Thousands marker'),
      '#options' => array(
        '' => t('- None -'),
        ',' => t('Comma'),
        ' ' => t('Space'),
        '.' => t('Decimal'),
        '\'' => t('Apostrophe'),
      ),
      '#default_value' => $this->options['separator'],
      '#description' => t('What single character to use as the thousands separator.'),
      '#size' => 2,
    );
    $form['prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Prefix'),
      '#default_value' => $this->options['prefix'],
      '#description' => t('Text to put before the number, such as currency symbol.'),
    );
    $form['suffix'] = array(
      '#type' => 'textfield',
      '#title' => t('Suffix'),
      '#default_value' => $this->options['suffix'],
      '#description' => t('Text to put after the number, such as currency symbol.'),
    );
  }

  function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);
  }

  function render($empty = FALSE) {
    if ($empty) {
      return '';
    }

    // Calculate the total value.
    $value = 0;
    foreach ($this->view->result as $cart_line) {
      $value += $cart_line->field_flagging_cart_quantity[0]['raw']['value'] * $cart_line->field_flagging_cart_product_price[0]['raw']['value'];
    }
    $value = number_format($value, 2, '.', $this->options['separator']);

    // Add the label if not empty.
    $label = $this->sanitize_value($this->options['display_label'], 'xss');
    if (!empty($label)) {
      $label = '<span class="cart-total-label">' . $label . '</span>';
    }

    // Return the value.
    return $label
      . $this->sanitize_value($this->options['prefix'], 'xss')
      . $this->sanitize_value($value)
      . $this->sanitize_value($this->options['suffix'], 'xss');
  }
}
